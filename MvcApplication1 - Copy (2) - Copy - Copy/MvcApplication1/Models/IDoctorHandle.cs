﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcApplication1.Models
{
    public interface IDoctorHandle
    {
        void addDr(Doctor d);
        List<Doctor> AllDoctors();
        void editDr(Doctor d);
        Doctor GetDrbyId(int id);
        void delDr(int id);
    }
}
