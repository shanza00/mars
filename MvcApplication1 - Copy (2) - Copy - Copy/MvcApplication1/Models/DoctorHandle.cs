﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcApplication1.Models;

namespace MvcApplication1.Models
{
    public class DoctorHandle:IDoctorHandle
    {
        MARSEntities db = new MARSEntities();
        public void addDr(Doctor d)
        {
            db.Doctors.Add(d);
            db.SaveChanges();

        }
        public List<Doctor> AllDoctors()
        {
            return db.Doctors.ToList();
        }
        public void editDr(Doctor d)
        {
            Doctor dr = db.Doctors.Find(d.Id);
            dr.FirstName = d.FirstName;
            dr.MiddleName = d.MiddleName;
            dr.LastName = d.LastName;
            dr.Qualification = d.Qualification;
            dr.Specialization = d.Specialization;
            dr.VisitingAddressAfternoon = d.VisitingAddressAfternoon;
            dr.VisitingAddressMorning = d.VisitingAddressMorning;
            dr.Email = d.Email;
            dr.PhoneNo = d.PhoneNo;
            dr.Designation = d.Designation;

            db.SaveChanges();
        }
        public Doctor GetDrbyId(int id)
        {
            Doctor d= db.Doctors.Find(id);
            return d;
        }
        public void delDr(int id)
        {
            Doctor d = db.Doctors.Find(id);
            db.Doctors.Remove(d);
            db.SaveChanges();
        }
    }
}