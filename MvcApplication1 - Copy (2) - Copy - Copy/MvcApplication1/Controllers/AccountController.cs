﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        IAdminHandle ah;

        public AccountController(IAdminHandle h)
        {
            ah=h;
        }

        public ActionResult login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult loginverify(string username,string password)
        {
            if (ah.loginVerify(username, password))
            {
                Session["admin"] = username;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("login");
            }
        }
        public ActionResult logout()
        {
            
            Session["admin"] = null;
            
            return RedirectToAction("login", "Account");
        }

    }
}
