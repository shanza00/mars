﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    public class CategoryApiController : ApiController
    {
        MARSEntities db = new MARSEntities();
        

        

        [Route("api/Categoryapi/getcategorybypatientid")]
        public List<Category> GetCategoryByPatientId(int PatientId)
        {
            var query = from x in db.Categories where x.PatientId == PatientId select x;
            return query.ToList();
        }

        [Route("api/Categoryapi/postcategory")]
        [HttpPost]
        public object PostCategory(Category c)
        {
            db.Categories.Add(c);
            db.SaveChanges();
            return new {CategoryId=c.CategoryId };
        }
        
        [Route("api/Categoryapi/updatecategory")]
        [HttpPost]
        public void UpdateCategory(Category c)
        {
            Category c1 = db.Categories.Find(c.CategoryId);
            
            c1.Name = c.Name;
            c1.PatientId = c.PatientId;
            db.SaveChanges();
        }
        [Route("api/Categoryapi/deletecategory")]
        public void DeleteCategory(int CategoryId)
        {
            Category c = db.Categories.Find(CategoryId);
            db.Categories.Remove(c);
            db.SaveChanges();
        }
        

        [Route("api/Categoryapi/getcategories")]
        
        public List<Category> GetCategories()
        {
            return db.Categories.ToList();

            
        }
        
    }
}
