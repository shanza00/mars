﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    public class DoctorController : Controller
    {
        //
        // GET: /Doctor/
        IDoctorHandle dh;
        public DoctorController(IDoctorHandle h)
        {
            dh = h;
        }

        public ActionResult Doctors()
        {
            if (Session["admin"] != null)
                return View(dh.AllDoctors());
            else
                return RedirectToAction("login", "Account");
        }
        public ActionResult AddDoctor()
        {
            return View();
        }
        public ActionResult EditDoctor(int id)
        {
            return View(dh.GetDrbyId(id));
        }
        public ActionResult EditDr(Doctor d)
        {
            dh.editDr(d);
            return RedirectToAction("Doctors", "Doctor");
        }
        public ActionResult DelDr(int id)
        {
            dh.delDr(id);
            return RedirectToAction("Doctors", "Doctor");
        }
        public ActionResult AddDr(Doctor d)
        {

            dh.addDr(d);

            return RedirectToAction("Doctors","Doctor");

        }

    }
}
