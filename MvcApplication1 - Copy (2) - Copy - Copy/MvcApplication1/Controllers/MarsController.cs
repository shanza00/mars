﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Mvc;
using MvcApplication1.Models;


namespace MvcApplication1.Controllers
{

    
    public class MarsController : ApiController
    {
        MARSEntities db = new MARSEntities();

        [Route("api/mars/get")]
        
        public Doctor Get()
        {
            return db.Doctors.Find(2);

        }

        [Route("api/mars/getall")]
        public List<Doctor> Getall()
        {

            return db.Doctors.ToList();
        }

        [HttpPost]
        [Route("api/mars/post")]
        public void post(Doctor d)
        {
            db.Doctors.Add(d);
            db.SaveChanges();
        }

        
    }
}
