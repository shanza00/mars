﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MvcApplication1.Models;
using AttributeRouting.Web.Mvc;

namespace MvcApplication1.Controllers
{
    public class RecordApiController : ApiController
    {
        MARSEntities db = new MARSEntities();

        [Route("api/recordapi/postrecord")]
        public void PostRecord(Record r)
        {
            db.Records.Add(r);
            db.SaveChanges();
        }

    }
}
