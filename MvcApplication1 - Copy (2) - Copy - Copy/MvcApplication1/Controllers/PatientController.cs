﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    public class PatientController : Controller
    {
        //
        // GET: /Patient/

        public ActionResult PatientRecords()
        {
            if (Session["admin"] != null)
                return View();
            else
                return RedirectToAction("login", "Account");
        }
        public ActionResult MedicalRecords()
        {
            if (Session["admin"]!=null)
                return View();
            else
                return RedirectToAction("login", "Account");
        }
        public ActionResult UserPatient()
        {
            if (Session["admin"]!=null)
                return View();
            else
                return RedirectToAction("login", "Account");
        }
    }
}
