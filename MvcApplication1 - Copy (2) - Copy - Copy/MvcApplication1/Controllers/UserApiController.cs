﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    public class UserApiController : ApiController
    {
        MARSEntities db=new MARSEntities();

        

        [Route("api/userapi/postuser")]
        [HttpPost]
        public void PostUser(User u)
        {
            db.Users.Add(u);
            db.SaveChanges();
            
        }
        [Route("api/userapi/verifyuser")]
        [AcceptVerbs("Get","post")]
        public object VerifyUser(string Email)
        {
            var query = from x in db.Users where x.Email == Email select x;
            if (query.Count() != 0)
            {
                return new { result = "Yes" };
            }
            else
            {
                return new { result = "No" };
            }
        }
        
    }
}
