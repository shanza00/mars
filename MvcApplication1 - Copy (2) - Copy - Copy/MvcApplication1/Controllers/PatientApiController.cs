﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Mvc;
using MvcApplication1.Models;

namespace MvcApplication1.Controllers
{
    public class PatientApiController : ApiController
    {
        MARSEntities db = new MARSEntities();

    
        

        [Route("api/patientapi/postpatient")]
        public object PostPatient(string Name, Byte[] Picture, string Email)
        {
            Patient p = new Patient();

            db.Patients.Add(p);
            p.Picture = Picture;
            p.Name = Name;
            var query0 = from x in db.Users where x.Email == Email select x;
            p.UserId = query0.First().Id;

            db.SaveChanges();
            int id = p.PatientId;
            return new {PatientId = id };
            

        }

        [Route("api/patientapi/deletepatient")]
        public void DeletePatient(int id)
        {
            Patient p = db.Patients.Find(id);
            db.Patients.Remove(p);
            db.SaveChanges();
        }
        [Route("api/patientapi/updatepatient")]
        [HttpPost]
        public void UpdatePatient(Patient p)
        {
            Patient p1 = db.Patients.Find(p.PatientId);
            p1.Name = p.Name;
            p1.Picture = p.Picture;
            p1.UserId = p.UserId;
        }

        [Route("api/patientapi/getbyuserid")]
        public List<Patient> GetbyUserId(string Email)
        {
            var query0 = from x in db.Users where x.Email == Email select x;
            int id = query0.First().Id;
            var query = from x in db.Patients where x.UserId == id select x;

            return query.ToList();

        }

        [Route("api/patientapi/getbypatientids")]
        public Patient GetbyPatientId(int id)
        {
            return db.Patients.Find(id);
        }
        
    }
}
